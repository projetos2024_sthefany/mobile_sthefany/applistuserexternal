import React from "react";
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import UsersList from "./src/usersList";
import UsersDetails from "./src/usersDetails";

 const Stack = createStackNavigator();
 
  export default function App() {
  return (
     <NavigationContainer>
      <Stack.Navigator initialRouteName="UsersList">
        <Stack.Screen name="UsersList" component={UsersList}/>
        <Stack.Screen name="Detalhes" component={UsersDetails}/>
      </Stack.Navigator>
     </NavigationContainer>
  );
}
 
