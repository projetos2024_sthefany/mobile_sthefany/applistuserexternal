import React from "react";
import { useEffect, useState } from "react";
import { View, Text, FlatList,TouchableOpacity } from "react-native";
import axios from "axios";

const UsersList = ({ navigation }) => {
  const [users, setUsers] = useState([]);
  useEffect(() => {
    getUsers();
  }, []);

  async function getUsers() {
    try {
      const response = await axios.get(
        "https://jsonplaceholder.typicode.com/users"
      ); //REQUISIÇÃO (VEM PARA A RESPONSE)
      //SOLICITAÇAO PARA A API DE FORA
      setUsers(response.data);
    } catch (error) {
      console.error(error);
    }
  }
  const UserPress = (user) => {
    navigation.navigate("Detalhes", { user });
  };

  return (
    <View>
      <Text>Lista de Usuários</Text>
      <FlatList
        data={users}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({ item }) => (
          <View>
            <TouchableOpacity onPress={() => UserPress(item)}>
            <Text>Nome: {item.name}</Text>
            </TouchableOpacity>
          </View>
        )}
      />
    </View>
  );
};

export default UsersList;
